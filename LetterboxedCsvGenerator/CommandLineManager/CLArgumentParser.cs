﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace CommandLineManager
{
    public class CLArgumentParser {

        private Dictionary<string, Action<string>> callbacks = new Dictionary<string, Action<string>>();
        public Dictionary<string, string> ParseCommandLineArguments(string[] args)
        {
            var result = new Dictionary<string, string>();
            for(int i = 0; i < args.Length;++i)
            {
                var command = args[i];
                if(args[i][0] == '-' && callbacks.ContainsKey(command))
                {
                    var isLastArgument = i == args.Length - 1;
                    var noOptions = isLastArgument ? true  : args[i + 1][0] == '-';
                    if (isLastArgument || noOptions)
                    {
                        result[command] = String.Empty;
                    }
                    else
                    {
                        result[command] = args[i + 1];
                    }

                    callbacks[command](result[command]);
                }
            }

            return result;
        }

        public void ParseCommandLineArguments(string[] args,  object commandLineOptions)
        {
            var properties = commandLineOptions.GetType().GetTypeInfo().GetProperties();
            foreach(var prop in properties)
            {
                OptionAttribute attribute = prop.GetCustomAttribute<OptionAttribute>();
                if (attribute == null) continue;
                var indexOf = Array.FindIndex(args, arg => arg == $"-{attribute.shortCmd}");
                string value = String.Empty;
                if (indexOf >= 0)
                {
                    var isLastArgument = indexOf == args.Length - 1;
                    var noOptions = isLastArgument ? true : args[indexOf + 1][0] == '-';
                    if (isLastArgument || noOptions)
                    {
                        value = String.Empty;
                    }
                    else
                    {
                        value = args[indexOf + 1];
                    }

                    prop.SetValue(commandLineOptions, Convert.ChangeType(value, prop.PropertyType), null);
                }

            }
        }

    }
}
